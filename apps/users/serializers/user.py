from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import update_last_login, Group
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.settings import api_settings

from users.models import User


class CheckTokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=255)




class UserDataSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False, validators=[validate_password])


    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'phone',
            'language',
            'password',
        ]

    def validate(self, attrs):
        if password := attrs.get('password'):
            attrs['password'] = make_password(password)
        return attrs


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user_data'] = UserDataSerializer(self.user).data

        if api_settings.UPDATE_LAST_LOGIN:
            update_last_login(None, self.user)

        return data


class UserModelSerializer(serializers.ModelSerializer):
    language = serializers.ChoiceField(choices=User.LANGUAGES_CHOICES, allow_blank=True, allow_null=True,
                                       required=False)
    password = serializers.CharField(max_length=255, required=False, write_only=True)
    password_confirm = serializers.CharField(max_length=255, required=False, write_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'phone',
            'username',
            'first_name',
            'last_name',
            'email',
            'language',
            'password',
            'password_confirm',
        ]
        extra_kwargs = dict(
            password=dict(required=True),
        )

    @transaction.atomic
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        password_confirm = validated_data.pop('password_confirm', None)

        if password == password_confirm:
            if password:
                user = super().create(validated_data)
                user.set_password(password)
                user.save()
            else:
                raise serializers.ValidationError('Password is required')
        else:
            raise serializers.ValidationError('Passwords must match each other')


        return user


class ChangePasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(max_length=128, write_only=True, required=True)

    def save(self, **kwargs):
        password = self.validated_data['new_password']
        user = self.context['request'].user
        user.set_password(password)
        user.save()
        return user

class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'phone',
            'username',
            'first_name',
            'last_name',
            'email',
            'language',

        ]