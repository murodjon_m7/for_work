from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView

from users.views.auth import AuthViewSet, LoginView
from users.views.user import UserViewSet, ChangePasswordView

router = DefaultRouter()
router.register('auth', AuthViewSet, 'auth')
router.register('users', UserViewSet, 'users')

urlpatterns = [
    path('login/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('login', LoginView.as_view(), name='login'),
    path('change-password', ChangePasswordView.as_view(), name='change-password'),
    path('', include(router.urls)),
]
