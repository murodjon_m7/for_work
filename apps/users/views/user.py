from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import UpdateAPIView

from users.models import User
from users.serializers.user import UserModelSerializer, UserListSerializer
from users.serializers.user import ChangePasswordSerializer

class UserViewSet(ModelViewSet):
    serializer_class = UserModelSerializer
    queryset = User.objects.order_by('-id')
    parser_classes = (MultiPartParser, JSONParser)
    filter_backends = [SearchFilter, DjangoFilterBackend]
    ordering = ['-id']
    search_fields = ['username', 'first_name']

    def get_paginated_response(self, data):
        return Response({
            'count': self.paginator.page.paginator.count,
            'next': self.paginator.page.next_page_number() if self.paginator.page.paginator.num_pages > self.paginator.page.number else None,
            'previous': self.paginator.page.previous_page_number() if self.paginator.page.number > 1 else None,
            'results': data
        })

    def get_serializer_class(self):
        return UserModelSerializer

class ChangePasswordView(UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    permission_classes = [IsAuthenticated, ]

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({"success": "Password updated successfully."})


    def get_serializer_class(self):
        if self.action == 'list':
            return UserListSerializer
        return UserModelSerializer